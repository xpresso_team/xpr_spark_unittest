"""
An example of how to use DataFrame for ML
"""
__author__ = "Naveen Sinha"

import os
import sys
import shutil
import tempfile
from pyspark.sql import SparkSession

if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

from one_hot_encoder.app.custom_tansformer import CustomerTransformer

if __name__ == "__main__":

    input_path = "data/sample_data.txt"

    spark = SparkSession \
        .builder \
        .appName("DataFrameExample") \
        .getOrCreate()

    # Load an input file
    print("Loading LIBSVM file with UDT from " + input_path + ".")
    df = spark.read.format("libsvm").load(input_path).cache()

    df = CustomerTransformer().transform(df)

    # Save the records in a parquet file.
    tempdir = tempfile.NamedTemporaryFile(delete=False).name
    os.unlink(tempdir)
    print("Saving to " + tempdir + " as Parquet file.")
    df.write.parquet(tempdir)
    # Load the records back.
    print("Loading Parquet file with UDT from " + tempdir)
    newDF = spark.read.parquet(tempdir)
    print("Schema from Parquet:")
    newDF.printSchema()
    spark.stop()
